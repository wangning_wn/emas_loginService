Pod::Spec.new do |s|

  s.name         = "LoginService"
  s.version      = "1.0.0"
  s.summary      = "LoginService类库"

  s.description  = <<-DESC
                   * Detail about LoginService framework.
                   DESC

  s.homepage     = "http://"
  s.license      = 'MIT (example)'
  s.author       = { "未定义" => "undefined" }
  s.platform     = :ios, '8.0'
  s.ios.deployment_target = '8.0'
  s.source       = { :http => "http://LoginService.zip" }
  s.preserve_paths = "LoginService.framework/*"
  s.resources  = "LoginService.framework/*.{bundle,xcassets}"
  s.vendored_frameworks = 'Login.framework'
  s.requires_arc = true
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/LoginService' }

  #s.dependency 'XXXX'

end
